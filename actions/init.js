/**
 * Initialize the repository.
 *
 * Takes the MTGJSON "AllPrintings.json" file and pares it down to just be the data needed for these scripts to operate on,
 * for faster script execution of other functions.
 */

const fs = require('fs');

module.exports = function(filename = '../AllPrintings.json') {
  const mtgJSON = require(filename).data;

  let trimmed = {};
  let setNameToCode = {};
  let uuidToCardMeta = {};

  Object.keys(mtgJSON).forEach(setCode => {
    let setData = mtgJSON[setCode];
    setNameToCode[setData.name] = setCode;

    // Delete attributes we don't use at the set/expansion level
    ['booster', 'boosterV3', 'sealedProduct', 'translations', 'meta'].forEach(propName => {
      delete setData[propName];
    });

    setData.cards = setData.cards.filter(card => {
      // Only keep cards that have a paper form
      return card.availability.indexOf('paper') >= 0 || card.borderColor == 'gold';
    }).map(card => {
      // Save "front" faces of a card as the primary card data when looking up by UUID
      if (card.layout !== 'transform' || card.side == 'a') {
        uuidToCardMeta[card.identifiers.scryfallId] = { set: setCode, name: card.name };
      }

      // Delete attributes we don't use on the card level
      [
        'borderColor', 'convertedManaCost', 'edhrecRank', 'foreignData', 'frameVersion', 'isMtgo', 'keywords',
        'legalities', 'originalText', 'originalType', 'power', 'printings', 'purchaseUrls', 'rulings',
        'subtypes', 'supertypes', 'toughness'
      ].forEach(propName => {
        delete card[propName];
      });

      // Keep only the most recent two price values
      if (typeof card.prices != 'undefined') {
        let priceTmp = { paper: {}, paperFoil: {} };
        if (typeof card.prices.paper != 'undefined' && card.prices.paper != null) {
          Object.keys(card.prices.paper).slice(-2).forEach(date => {
            priceTmp.paper[date] = card.prices.paper[date];
          });
        }
        if (typeof card.prices.paperFoil != 'undefined' && card.prices.paperFoil != null) {
          Object.keys(card.prices.paperFoil).slice(-2).forEach(date => {
            priceTmp.paperFoil[date] = card.prices.paperFoil[date];
          });
        }
        card.prices = priceTmp;
      }

      card.uuid = card.identifiers.scryfallId; // Don't use the MTGJSON-specific UUID, but fall back to the Scryfall one

      return card;
    });

    if (setCode == 'AMH1') {
      // In the Modern Horizons Art Card series, no need to keep the backs
      setData.tokens = setData.tokens.filter(token => token.side == 'a');
    }

    setData.tokens = setData.tokens.map(token => {
      uuidToCardMeta[token.identifiers.scryfallId] = { set: setCode, name: token.name };

      // Delete attributes we don't use at the token level
      [
        'borderColor', 'colorIdentity', 'power', 'toughness', 'type'
      ].forEach(propName => {
        delete token[propName];
      });

      // Tokens don't have variations set, so calculate them now
      let otherVariations = setData.tokens.filter(otherToken => {
        return (otherToken.identifiers.scryfallId !== token.identifiers.scryfallId && otherToken.name == token.name);
      });
      if (otherVariations.length > 0) {
        token.variations = otherVariations.map(t => t.identifiers.scryfallId);
      }

      return token;
    });

    trimmed[setCode] = setData;
  });

  let writeMinFile = new Promise((resolve, reject) => {
    fs.writeFile('lib/AllPrintings.min.json', JSON.stringify(trimmed, null, 2), err => {
      if (err) {
        reject(err);
        return;
      }
      resolve('Done!');
    });
  })


  let writeMetaFile = new Promise((resolve, reject) => {
    fs.writeFile('lib/AllPrintings.meta.json', JSON.stringify({
      setNameToCode,
      uuidToCardMeta
    }, null, 2), err => {
      if (err) {
        reject(err);
      }
      resolve('Done!');
    });
  });

  return Promise.all([writeMinFile, writeMetaFile]);
};
