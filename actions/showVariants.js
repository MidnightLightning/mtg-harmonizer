/**
 * Create HTML galleries of card variants.
 *
 * Within a given set, cards that have the same name, but different artwork/treatment
 * are considered "Variants". In modern sets, these can be differentiated
 * by "collector number", but some older sets don't have consistent numbering for that.
 *
 * This script creates visual galleries for any cards that have "variants" in a set,
 * and lists their UUID with the variant, as a canonical way to represent those cards.
 */

const mtgJSON = require('../lib/AllPrintings.min.json');
const fs = require('fs');

function buildGallery(setCode) {
  let setCards = mtgJSON[setCode].cards.filter(cardData => {
    if (cardData.side == 'b') return false;
    return (typeof cardData.variations !== 'undefined' && cardData.variations.length > 0);
  });
  let setTokens = mtgJSON[setCode].tokens.filter(tokenData => {
    return (typeof tokenData.variations !== 'undefined' && tokenData.variations.length > 0);
  });
  if (setCards.length === 0 && setTokens.length === 0) return;
  let html = [];
  html.push('<html><head><title>Visual Variants</title>');
  html.push('<style>');
  html.push('html, body { padding: 0; margin: 0; }');
  html.push('h1 { padding: 0.5rem; text-align:center; }');
  html.push('#mtg-cards-container { display: flex; flex-wrap: wrap; }');
  html.push('.mtg-card { padding: 0.5rem; }');
  html.push('.mtg-card img { display: block; margin: 0 auto; max-height:225px; }');
  html.push('.mtg-card-meta { margin: 0.5rem 0 2rem 0; }');
  html.push('.mtg-card-meta th { text-align: right; padding-right: 0.5rem; }');
  html.push('.uuid { font-size: 0.7rem; font-family: monospace; }');
  html.push('</style>');
  html.push('</head><body>');
  html.push('<h1>' + mtgJSON[setCode].name + '</h1>');
  html.push('<div id="mtg-cards-container">');

  setCards.forEach(cardData => {
    if (typeof cardData.variations !== 'undefined' && cardData.variations.length > 0) {
      // Show visual representations of these variants
      html.push('<div class="mtg-card">');
      html.push('<img src="https://api.scryfall.com/cards/' + cardData.identifiers.scryfallId + '?format=image&version=normal" />');
      html.push('<table class="mtg-card-meta"><tr><th>UUID</th><td class="uuid">' + cardData.identifiers.scryfallId + '</td></tr>');
      html.push('<tr><th>Name</th><td>' + cardData.name + '</td></tr>');
      html.push('<tr><th>Number</th><td>' + cardData.number + '</td></tr>');
      html.push('</table></div>');
    }
  });

  html.push('</div></body></html>');
  return html;
}

module.exports = function() {
  if (!fs.existsSync('variants')) {
    fs.mkdirSync('variants');
  }
  let outputPromises = [];
  Object.keys(mtgJSON).forEach(setCode => {
    let html = buildGallery(setCode);
    if (typeof html == 'undefined') {
      outputPromises.push(Promise.resolve(`${setCode} has no variants to show`));
      return;
    }

    outputPromises.push(new Promise((resolve, reject) => {
      fs.writeFile(`variants/${setCode}.html`, html.join("\n"), err => {
        if (err) {
          reject(err);
          return;
        }
        resolve(`${setCode} Done!`);
      });
    }));
  });
  return Promise.all(outputPromises);
};
