/**
 * Parse an input file of "haves" and change it to another format.
 *
 * "Have" lists are lists of cards a player has, which they're willing to trade away.
 * (a.k.a. "collection", or "tradelinst"). These cards will have an exact set
 * and grade value, since each represents a physical card the player has.
 *
 * This script takes a CSV input and converts it into a JSON output. The JSON objects
 * in the output will have a 'uuid' property added to them which indicates the global identifier that was analyzed for that input.
 */

const fs = require('fs'),
  path = require('path'),
  utils = require('../lib/formatUtils');

module.exports = function(inputFile, inputFormat, outputFormat) {
  if (typeof inputFile == 'undefined' || inputFile == '') {
    console.error('No input file specified');
    return;
  }

  return utils.parseInputFile(inputFile, inputFormat).then(data => {
    // Convert to export format
    let formatOut;
    try {
      formatOut = require('../formats/' + outputFormat).parseOutput;
    } catch (err) {
      if (err.code == 'MODULE_NOT_FOUND') {
        return Promise.reject(`Cannot find parser data for format ${outputFormat}`);
      } else {
        return Promise.reject(err);
      }
    }

    let out = formatOut(data);

    // Save the formatted output
    if (!fs.existsSync('out')) {
      fs.mkdirSync('out');
    }
    return new Promise((resolve, reject) => {
      fs.writeFile(
        `out/${path.parse(inputFile).name}.${out.extension}`,
        out.data,
        err => {
          if (err) {
            reject(err);
            return;
          }
          resolve(`${inputFile} ${inputFormat} => ${outputFormat} conversion complete`);
        }
      );
    });
  });
};
