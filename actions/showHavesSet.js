/**
 * Given a collection of "haves", show metadata about an individual set/expansion.
 */

const jsonParser = require('../lib/jsonParser'),
  utils = require('../lib/formatUtils');

function sumTradelistValues(cards) {
  return cards.reduce((runningTotal, card) => {
    return runningTotal + parseInt(card['Tradelist Count']);
  }, 0);
}
const colors = ['White', 'Blue', 'Black', 'Red', 'Green', 'Multicolor', 'Colorless'];
const colorMap = {
  W: 'White',
  U: 'Blue',
  B: 'Black',
  R: 'Red',
  G: 'Green'
};
function sortByTradelistCount(a, b) {
  return b['Tradelist Count'] - a['Tradelist Count'];
}

module.exports = function(inputFile, inputFormat, setName) {
  if (typeof inputFile == 'undefined' || inputFile == '') {
    console.error('No input file specified');
    return;
  }
  console.log(`Analyzing cards from "${setName}"...`);

  return utils.parseInputFile(inputFile, inputFormat).then(data => {
    // Filter down the cards to the selected set
    data = data.filter(card => {
      if (typeof card.uuid == 'undefined') return false;

      let cardSet = jsonParser.findSetByUuid(card.uuid);
      return cardSet.name.toLowerCase() === setName.toLowerCase()
        || cardSet.code.toLowerCase() == setName.toLowerCase();
    });

    let meta = {};
    ['Creature', 'Enchantment', 'Artifact', 'Instant', 'Sorcery', 'Planeswalker'].forEach(t => {
      meta[t] = {};
      colors.forEach(color => {
        meta[t][color] = [];
      });
    });
    meta.Land = [];

    data.forEach(card => {
      let cardData = jsonParser.findCardByUuid(card.uuid);
      if (cardData === false) {
        cardData = jsonParser.findTokenByUuid(card.uuid);
        if (cardData === false) return;
        // Card is a Token

      } else {
        // Card is a non-Token
        let cardType;
        if (cardData.types.length > 1) {
          if (cardData.type.indexOf('Artifact Creature') === 0) {
            cardType = 'Creature';
          } else {
            console.log(cardData);
            process.exit();
          }
        } else {
          cardType = cardData.types[0];
        }
        if (cardType !== 'Land') {
          // Non-land card
          let cardColor;
          if (cardData.colors.length == 0) {
            cardColor = 'Colorless';
          } else if (cardData.colors.length > 1) {
            cardColor = 'Multicolor';
          } else {
            cardColor = colorMap[cardData.colors[0]];
          }
          if (typeof meta[cardType][cardColor] == 'undefined') {
            console.log(`${cardData.name}: ${cardColor}, ${cardType} (${card['Tradelist Count']})`);
            process.exit();
          }
          meta[cardType][cardColor].push(Object.assign({}, card, { Type: cardData.type, Rarity: cardData.rarity }));
        } else {
          // Land card
          //console.log(`${cardData.name}: ${cardType} (${card['Tradelist Count']})`);
          meta.Land.push(Object.assign({}, card, { Type: cardData.type, Rarity: cardData.rarity }));
        }
      }
    });

    // Show results
    Object.keys(meta).forEach(cardType => {
      console.log('');
      console.log(cardType);
      console.log('==========');
      if (cardType !== 'Land') {
        // Non-land cards
        colors.forEach(color => {
          if (meta[cardType][color].length > 0) {
            let count = sumTradelistValues(meta[cardType][color]);
            console.log(`${color}: ${count} cards`);
            meta[cardType][color].sort(sortByTradelistCount).forEach(card => {
              console.log(`${card.Name}: ${color}, ${card.Type}, ${card.Rarity} (${card['Tradelist Count']})`);
            });
            console.log('');
          }
        });
      } else {
        // Land cards
        let count = sumTradelistValues(meta[cardType]);
        console.log(`${count} cards`);
        meta[cardType].sort(sortByTradelistCount).forEach(card => {
          console.log(`${card.Name}: ${card.Type}, ${card.Rarity} (${card['Tradelist Count']})`);
        })
      }
    });
  });
};
