/**
 * Show a summary of MTGJSON data for a given set/expansion.
 */

const mtgJSON = require('../lib/AllPrintings.min.json');

function showSet(setCode) {
  if (typeof mtgJSON[setCode] == 'undefined') {
    console.error(`Cannot find data on set ${setCode}`);
    console.log('Available sets are:');
    Object.keys(mtgJSON).forEach(setCode => {
      console.log(`${setCode}: ${mtgJSON[setCode].name}`);
    });
    return;
  }
  console.log('');
  console.log(`${mtgJSON[setCode].name} (${setCode})`);
  console.log('==========');
  mtgJSON[setCode].cards.forEach(card => {
    console.log(`${card.name}: ${card.number}, ${card.uuid}`);
  });
  console.log('');
  mtgJSON[setCode].tokens.forEach(card => {
    console.log(`${card.name}: ${card.number}, ${card.uuid}`);
  });
}

module.exports = function(setCode) {
  if (typeof setCode == 'undefined' || setCode == '') {
    // Show all sets
    Object.keys(mtgJSON).forEach(showSet);
  } else {
    // Show just that one set
    showSet(setCode.toUpperCase());
  }
};
