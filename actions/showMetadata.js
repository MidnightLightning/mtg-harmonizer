/**
 * Show different metadata stats about the MTGJSON data.
 */

const mtgJSON = require('../lib/AllPrintings.min.json');

module.exports = function() {
  let frameEffects = {};

  Object.keys(mtgJSON).forEach(setCode => {
    mtgJSON[setCode].cards.forEach(card => {
      if (typeof card.frameEffects != 'undefined') {
        card.frameEffects.forEach(style => {
          frameEffects[style] = true;
        });
      }
    });
  });

  return Object.keys(frameEffects).sort();
};
