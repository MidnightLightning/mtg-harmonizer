const jsonParser = require('../lib/jsonParser'),
  setMapping = require('./deckbox-sets.json'),
  cardMapping = require('./deckbox-cards.json');

module.exports = {
  parseInput: function(cards) {
    return cards.map(card => {
      // First check if the card is a unique card we know the UUID of already
      for (let i = 0; i < cardMapping.length; i++) {
        if (
          cardMapping[i].name == card['Name']
          && cardMapping[i].setName == card['Edition']
          && (typeof cardMapping[i].number == 'undefined' || cardMapping[i].number == card['Card Number'])
        ) {
          card.uuid = cardMapping[i].uuid;
          return card;
        }
      }

      // See if the set name needs to be updated, or if it can be used directly
      let setName = card['Edition'];
      if (setName.indexOf('Oversized:') === 0) {
        setName = setName.substr(11) + ' Oversized';
      } else if (typeof setMapping[setName] !== 'undefined') {
        setName = setMapping[setName]
      }

      let cardData;
      let multiverseId;
      if (card['Image URL']) {
        // The path that Deckbox uses for the image includes the Multiverse ID
        let urlParts = card['Image URL'].split('/')
        multiverseId = urlParts[urlParts.length - 1].split('.')[0];
      }
      if (setName.indexOf('Extras: ') === 0) {
        setName = setName.substr(8);
        // This is a token, not a card
        // Check to see if this is an emblem card
        if (card['Name'].indexOf('Emblem: ') == 0) {
          // This is an emblem; switch the label to the end
          card['Name'] = card['Name'].substr(8) + ' Emblem';
        }
        if (card['Name'].indexOf('Art Card: ') == 0) {
          // This is an "Art Card" layout
          card['Name'] = card['Name'].substr(10);
          card['Name'] = card['Name'] + ' // ' + card['Name'];
          if (setName == 'Midnight Hunt Art Series') {
            // Midnight Hunt art cards have a 's' after their number
            card['Card Number'] = card['Card Number'] + 's';
          }
        } else if (card['Name'] == 'Double-Faced Card Placeholder') {
          // These cards officially got designated as a separate "set"
          card['Name'] = 'Double-Faced Substitute Card';
          setName = 'Zendikar Rising Substitute Cards';
        }

        if (multiverseId) {
          cardData = jsonParser.findTokenByMultiverseNumber(setName, multiverseId);
        }
        if (!cardData) {
          cardData = jsonParser.findTokenByName(setName, card['Name'], card['Card Number']);
        }
      } else {
        // This is a standard card from a set
        // Check to see if this is a split card
        if (card['Name'].indexOf(' // ') > 0) {
          // This is a split card; MTGJSON lists these by their individual halves, so just look up the first part
          card['Name'] = card['Name'].split(' // ')[0];
        }
        if (multiverseId) {
          cardData = jsonParser.findCardByMultiverseNumber(setName, multiverseId);
        }
        if (!cardData) {
          cardData = jsonParser.findCardByName(setName, card['Name'], card['Card Number']);
        }
      }
      if (cardData === false) {
        console.error(`Failed to find card info for ${card['Name']} (${card['Card Number']}) in ${setName}`);
        return card;
      }

      card.uuid = cardData.identifiers.scryfallId;
      return card;
    });
  }
};
