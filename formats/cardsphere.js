/**
 * Parser logic for Cardsphere.
 *
 * https://cardsphere.com
 */

const csvParser = require('../lib/csvParser'),
  jsonParser = require('../lib/jsonParser'),
  utils = require('../lib/formatUtils'),
  setMapping = require('./cardsphere-sets.json'),
  cardMapping = require('./cardsphere-cards.json');

module.exports = {
  parseOutput: function(cards) {
    let columns = ['Count', 'Tradelist Count', 'Name', 'Edition', 'Card Number', 'Condition', 'Language', 'Foil'];

    let data = cards.map(card => {
      let out = {};
      columns.forEach(column => out[column] = card[column]);

      if (typeof card.uuid == 'undefined' || card.uuid == '') {
        // No UUID set; have to use just hope the columns are already set
        return out;
      }

      // Look up MTGJSON data by UUID
      let cardMeta = jsonParser.findByUuid(card.uuid);
      if (cardMeta === false || cardMeta.cardData === false) {
        console.error(`Failed to look up information about ${card.uuid}`, card);
        process.exit();
      }
      if (cardMeta.cardData.layout == 'transform') {
        out['Name'] = csvParser.cleanCardName(cardMeta.cardData.faceName);
      } else {
        out['Name'] = csvParser.cleanCardName(cardMeta.cardData.name);
      }
      out['Card Number'] = cardMeta.cardData.number;
      out['Edition'] = cardMeta.cardData.set.name;

      // See if the Edition needs to be updated or if it can be used directly
      if (out['Edition'].indexOf('Duel Decks Anthology:') === 0) {
        out['Edition'] = 'Duel Decks: Anthology';
      }
      if (out['Edition'].indexOf('Core Set') === 0) {
        out['Edition'] = 'Core' + out['Edition'].substr(8);
      }
      let tmp = utils.reverseSetLookup(setMapping, out['Edition']);
      if (tmp !== false) {
        out['Edition'] = tmp;
      }

      let cardType = cardMeta.cardData.type;
      if (typeof cardType !== 'undefined'
        && (cardType.indexOf('Basic Land') === 0 || cardType.indexOf('Land') === 0)
        && typeof cardMeta.cardData.variations !== 'undefined'
        && cardMeta.cardData.variations.length > 0
      ) {
        // Lands with variants by default get their collector number added as a suffix
        out['Name'] = `${out['Name']} (#${cardMeta.cardData.number})`;
      } else if (cardMeta.isToken) {
        // By default, tokens are indcated on CardSphere by having "Token" on the end of their name
        if (out['Name'].substr(-5) !== 'Token') {
          out['Name'] = out['Name'] + ' Token';
        }
        if (typeof cardMeta.cardData.variations !== 'undefined'
          && cardMeta.cardData.variations.length > 0
        ) {
          // Tokens with variants get their collector number added as a suffix
          out['Name'] = `${out['Name']} (#${cardMeta.cardData.number})`;
        }
      }

      // Look up in special cards to see if there's overrides
      for (let i = 0; i < cardMapping.length; i++) {
        if (cardMapping[i].uuid == card.uuid) {
          out['Name'] = cardMapping[i].name;
          out['Edition'] = cardMapping[i].setName;
          //console.log(`Found custom card info for ${card.uuid}:`, out);
        }
      }

      return out;
    });

    return {
      data: csvParser.objectsToCsv(columns, data),
      extension: 'csv'
    };
  }
};
