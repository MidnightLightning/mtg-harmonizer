/**
 * Parser for exporting raw JSON data.
 */

module.exports = {
  parseOutput: function(cards) {
    return {
      data: JSON.stringify(cards, null, 2),
      extension: 'json'
    };
  }
};
