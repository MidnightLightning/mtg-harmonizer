/**
 * Parser logic for Pucatrade.
 *
 * https://pucatrade.com
 */

const csvParser = require('../lib/csvParser'),
  jsonParser = require('../lib/jsonParser'),
  cardMapping = require('./pucatrade-cards.json');

module.exports = {
  parseOutput: function(cards) {
    let columns = ['Count', 'Name', 'Expansion', 'Foil', 'Condition', 'Language', 'PucaID', 'Status'];

    let data = cards.map(card => {
      let out = {};
      columns.forEach(column => out[column] = card[column]);
      out['Status'] = 'HAVE';

      if (typeof card.uuid == 'undefined' || card.uuid == '') {
        // No UUID set; have to use just hope the columns are already set
        return out;
      }

      // Look up MTGJSON data by UUID
      let cardMeta = jsonParser.findByUuid(card.uuid);
      if (cardMeta === false || cardMeta.cardData === false) {
        console.error(`Failed to look up information about ${card.uuid}`, card);
        process.exit();
      }
      out['Name'] = csvParser.cleanCardName(cardMeta.cardData.name);
      out['Expansion'] = cardMeta.cardData.set.name;

      let cardType = cardMeta.cardData.type;
      if (typeof cardType !== 'undefined'
        && (cardType.indexOf('Basic Land') === 0 || cardType.indexOf('Land') === 0)
        && typeof cardMeta.cardData.variations !== 'undefined'
        && cardMeta.cardData.variations.length > 0
      ) {
        // Lands with variants by default get their collector number added as a suffix
        out['Name'] = `${out['Name']} (${cardMeta.cardData.number})`;
      }

      // Look up in special cards to see if there's overrides
      for (let i = 0; i < cardMapping.length; i++) {
        if (cardMapping[i].uuid == card.uuid) {
          out['Name'] = cardMapping[i].name;
          out['Expansion'] = cardMapping[i].expansion;
          //console.log(`Found custom card info for ${card.uuid}:`, out);
        }
      }

      return out;
    });

    return {
      data: csvParser.objectsToCsv(columns, data),
      extension: 'csv'
    };
  }
};
