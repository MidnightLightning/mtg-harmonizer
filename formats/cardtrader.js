const csvParser = require("../lib/csvParser"),
  jsonParser = require('../lib/jsonParser');

module.exports = {
  parseOutput: function(cards) {
    let columns = ['Item Name', 'Quantity', 'Expansion Code', 'Expansion Name', 'Selling Price (in cents)', 'Language', 'Condition', 'Foil', 'Altered', 'Collector Number', 'Scryfall ID']

    let data = cards.map(card => {
      let out = {
        'Item Name': card['Name'],
        'Quantity': card['Count'],
        'Expansion Name': card['Edition'],
        'Selling Price (in cents)': Math.floor(parseFloat(card['My Price'].substr(1))*100),
        'Language': card['Language'],
        'Condition': card['Condition'],
        'Foil': card['Foil'],
        'Altered': card['Altered Art'],
        'Collector Number': card['Card Number'],
        'Scryfall ID': card['uuid']
      }

      if (out['Condition'] == 'Good (Lightly Played)') {
        // Naming difference in condition
        out['Condition'] = 'Slightly Played'
      } else if (out['Condition'] == '') {
        // Condition not specified; default to slightly played
        out['Condition'] = 'Slightly Played'
      }
      if (out['Language'] == '') {
        // Language not specified; default to English
        out['Language'] = 'en'
      }

      if (typeof card.uuid == 'undefined' || card.uuid == '') {
        // No UUID set; that's the best we can do
        return out;
      }

      // Look up MTGJSON data by UUID
      let cardMeta = jsonParser.findByUuid(card.uuid);
      if (cardMeta === false || cardMeta.cardData === false) {
        console.error(`Failed to look up information about ${card.uuid}`, card);
        process.exit();
      }

      out['Collector Number'] = cardMeta.cardData.number;
      out['Expansion Name'] = cardMeta.cardData.set.name;
      out['Expansion Code'] = cardMeta.cardData.set.code;

      return out;
    })

    return {
      data: csvParser.objectsToCsv(columns, data),
      extension: 'csv'
    }
  }
}