const fs = require('fs');
const Tokenizer = require('./tokenizer');

module.exports = {

  /**
   * Remove accented characters from card name.
   */
  cleanCardName: function(name) {
  return name
    .replace(/æ/g, 'ae')
    .replace(/â/g, 'a')
    .replace(/û/g, 'u')
    .replace(/ö/g, 'o')
    .replace(/á/g, 'a')
    .replace(/é/g, 'e')
    .replace(/í/g, 'i')
    .replace(/ú/g, 'u')
    .replace(/à/g, 'a')
    .replace(/®/g, '');
  },

  tokenizer: new Tokenizer([
    {
      regex: /"/,
      name: 'DOUBLE_QUOTE'
    },
    {
      regex: /,/,
      name: 'COMMA'
    },
    {
      regex: /\s+/,
      name: 'WHITESPACE'
    },
    {
      regex: /\S/,
      name: 'CHARACTER'
    }
  ]),

  csvStringLex: function(tokens) {
    let output = [];
    let index = 0;
    while (true) {
      if (tokens[index].type == 'DOUBLE_QUOTE') {
        // Start of quoted string; find the ending
        let foundClosing = false;
        for (let j = index+1; j < tokens.length; j++) {
          if (tokens[j].type == 'DOUBLE_QUOTE' && (j+1 >= tokens.length || tokens[j+1].type != 'DOUBLE_QUOTE')) {
            // Found the end of this string
            let children = tokens.slice(index+1, j);
            output.push({
              type: 'STRING',
              value: children.map(token => token.value).join('')
            });
            index = j;
            foundClosing = true;
            j = tokens.length+10; // Break out of inner group
          }
        }
        if (foundClosing === false) {
          // No closing quote? Treat as text instead
          output.push(tokens[index]);
        }
      } else {
        // Not a string grouping
        output.push(tokens[index]);
      }
      index++;
      if (index >= tokens.length) {
        return output;
      }
    }
  },

  csvSplit: function(tokens) {
    let output = [];
    let lastComma = -1;

    for (let index = 0; index < tokens.length; index++) {
      if (tokens[index].type == 'COMMA') {
        output.push(
          tokens.slice(lastComma+1, index)
            .map(token => token.value)
            .join('')
            .trim()
          );
        lastComma = index;
      }
    }

    // Add the last chunk of tokens
    if (tokens[tokens.length-1].type !== 'COMMA') {
      output.push(
        tokens.slice(lastComma+1)
          .map(token => token.value)
          .join('')
          .trim()
        );
    }
    return output;
  },

  /**
   * Take an array of values, and turn it into a row of CSV data.
   */
  toCsv: function(values) {
    return values.map(value => {
      if ((''+value).indexOf(',') >= 0) {
        return `"${value}"`;
      }
      return value;
    }).join(',');
  },

  /**
   * Parse CSV data.
   * Returns an array of arrays; for every line of the CSV file, outputs an array of
   * fields contained in that line.
   */
  parseCsv: function(data) {
    let rs = [];
    data.split("\n").map(line => {
      if (line == '') return;
      let raw;
      if (line.indexOf('"') < 0) {
        // No string quoting; just split the line
        raw = line.split(',').map(value => value.trim());
      } else {
        // Evaluate CSV escape sequences
        let tokens = this.tokenizer.parseText(line);
        tokens = this.csvStringLex(tokens);
        raw = this.csvSplit(tokens);
      }
      rs.push(raw);
    });
    return rs;
  },

  readCsvFile: function(filename) {
    return new Promise((resolve, reject) => {
      fs.readFile(filename, (err, data) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(this.parseCsv(data.toString('utf8')));
      });
    });
  },

  objectsToCsv: function(columns, dataArr) {
    let out = [this.toCsv(columns)];
    dataArr.forEach(dataObj => {
      if (typeof dataObj === 'undefined' || dataObj === false) return;
      out.push(this.toCsv(columns.map(columnName => {
        return dataObj[columnName];
      })));
    });
    return out.join("\n");
  }
};
