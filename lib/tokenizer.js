class Tokenizer {
  constructor(tokenList) {
    this.tokenList = tokenList;
  }

  // Loop through all the available tokens, finding the first one that matches
  findTokenForString(inputString) {
    for (let i = 0; i < this.tokenList.length; i++) {
      let match = inputString.match(this.tokenList[i].regex);
      if (match !== null && match.index == 0) {
        return {
          type: this.tokenList[i].name,
          value: match[0]
        };
      }
    }
    return false;
  }

  parseText(inputString) {
    if (inputString == '') {
      return [];
    }
    let index = 0;
    let tokens = [];
    while (true) {
      let token = this.findTokenForString(inputString.substr(index));
      if (token !== false) {
        tokens.push(token);
        index += token.value.length;
        if (index >= inputString.length) {
          return tokens;
        }
      } else {
        tokens.push({
          type: 'UNKNOWN',
          value: inputString.substr(index)
        });
        return tokens;
      }
    }
  }
}

module.exports = Tokenizer;
