const mtgJSON = require('./AllPrintings.min.json');
const mtgJSON_meta = require('./AllPrintings.meta.json');

module.exports = {
  csvToObjects: function(csvData) {
    const columns = csvData[0];
    let data = [];
    for (let i = 1; i < csvData.length; i++) {
      let record = {};
      columns.map((columnName, index) => {
        record[columnName] = csvData[i][index];
      });
      data.push(record);
    }
    return data;
  },

  findAllCardsByName: function(setName, cardName) {
    let code = mtgJSON_meta.setNameToCode[setName];
    if (typeof code === 'undefined') {
      console.error(`No set found with name "${setName}"`);
      return false;
    }

    return mtgJSON[code].cards.filter(cardData => {
      return cardData.name == cardName || cardData.faceName == cardName;
    });
  },

  findCardByName: function(setName, cardName, cardNumber) {
    let code = mtgJSON_meta.setNameToCode[setName];
    if (typeof code === 'undefined') {
      console.error(`No set found with name "${setName}"`);
      return false;
    }

    let setCards = mtgJSON[code].cards.filter(cardData => {
      return cardData.name == cardName || cardData.faceName == cardName;
    });
    if (setCards.length === 1) {
      // Single result found; return it
      return setCards[0];
    }
    if (setCards.length === 0) {
      return false;
    }
    // More than one card found
    // Try and narrow down by collector number
    let tmpCards = setCards.filter(cardData => {
      return cardData.number == cardNumber;
    });
    if (tmpCards.length === 1) {
      // Single result found; return it
      return tmpCards[0];
    }

    // Then try and narrow down by removing non-standard frame effects
    tmpCards = setCards.filter(cardData => {
      if (typeof cardData.promoTypes !== 'undefined') {
        return false;
      }
      if (typeof cardData.isAlternative !== 'undefined' && cardData.isAlternative) {
        return false;
      }
      if (typeof cardData.isStarter !== 'undefined' && cardData.isStarter) {
        return false;
      }
      if (typeof cardData.frameEffects !== 'undefined') {
        if (cardData.frameEffects.indexOf('showcase') >= 0
          || cardData.frameEffects.indexOf('extendedart') >= 0
          || cardData.frameEffects.indexOf('jpwalker') >= 0
        ) {
          return false;
        }
      }
      return true;
    });
    if (tmpCards.length === 1) {
      // Single result found; return it
      return tmpCards[0];
    }

    console.error(`More than one card with name "${cardName}" found in ${setName}`);
    return false;
  },

  findCardByNameAndFrameStyle: function(setName, cardName, frameStyle) {
    let code = mtgJSON_meta.setNameToCode[setName];
    if (typeof code === 'undefined') {
      console.error(`No set found with name "${setName}"`);
      return false;
    }

    let setCards = mtgJSON[code].cards.filter(cardData => {
      if (typeof cardData.frameEffects == 'undefined' || cardData.frameEffects.indexOf(frameStyle) < 0) {
        // Does not have the appropriate frame style
        return false;
      }
      return cardData.name == cardName || cardData.faceName == cardName;
    });
    if (setCards.length === 1) {
      // Single result found; return it
      return setCards[0];
    }
    if (setCards.length === 0) {
      return false;
    }

    console.error(`More than one card with name "${cardName}" found in ${setName}`);
    return false;
  },

  findCardByFlavorName: function(setName, cardName, flavorName) {
    let code = mtgJSON_meta.setNameToCode[setName];
    if (typeof code === 'undefined') {
      console.error(`No set found with name "${setName}"`);
      return false;
    }

    let setCards = mtgJSON[code].cards.filter(cardData => {
      if (typeof cardData.flavorName == 'undefined' || cardData.flavorName != flavorName) {
        // Does not have the appropriate flavor name
        return false;
      }
      return cardData.name == cardName || cardData.faceName == cardName;
    });
    if (setCards.length === 1) {
      // Single result found; return it
      return setCards[0];
    }
    if (setCards.length === 0) {
      return false;
    }

    console.error(`More than one card with name "${cardName}" found in ${setName}`);
    return false;
  },

  findCardByNumber: function(setName, cardNumber) {
    let code = mtgJSON_meta.setNameToCode[setName];
    if (typeof code === 'undefined') {
      console.error(`No set found with name "${setName}"`);
      return false;
    }

    let setCards = mtgJSON[code].cards.filter(cardData => {
      //console.log(`${cardData.number} ?= ${cardNumber}`);
      return cardData.number == cardNumber;
    });
    if (setCards.length === 1) {
      return setCards[0];
    }
    if (setCards.length === 0) {
      return false;
    }
    console.error(`More than one card with number "${cardNumber}" found in ${setName}`);
    return false;
  },

  findCardByMultiverseNumber: function(setName, multiverseId) {
    let code = mtgJSON_meta.setNameToCode[setName];
    if (typeof code === 'undefined') {
      console.error(`No set found with name "${setName}"`);
      return false;
    }

    let setCards = mtgJSON[code].cards.filter(cardData => {
      if (cardData.side && cardData.side != 'a') return false
      return cardData.identifiers.multiverseId == multiverseId
    });
    if (setCards.length === 1) {
      return setCards[0]
    }
    if (setCards.length === 0) {
      return false;
    }
    console.error(`More than one card with Multiverse ID "${multiverseId}" found in ${setName}`);
    return false;
  },

  findTokenByName: function(setName, tokenName, cardNumber) {
    let code = mtgJSON_meta.setNameToCode[setName];
    if (typeof code === 'undefined') {
      console.error(`No set found with name "${setName}"`);
      return false;
    }

    let setTokens = mtgJSON[code].tokens.filter(tokenData => {
      return tokenData.name == tokenName;
    });
    if (setTokens.length === 1) {
      return setTokens[0];
    }
    if (setTokens.length === 0) {
      return false;
    }
    // More than one card found; try and narrow down by collector number
    let tmpCards = setTokens.filter(tokenData => {
      if (tokenData.side == 'b') return false;
      return tokenData.number == cardNumber;
    });
    if (tmpCards.length === 1) {
      return tmpCards[0];
    }
    console.error(`More than one token with name "${tokenName}" found in ${setName}`);
    return false;
  },

  findTokenByMultiverseNumber: function(setName, multiverseId) {
    let code = mtgJSON_meta.setNameToCode[setName];
    if (typeof code === 'undefined') {
      console.error(`No set found with name "${setName}"`);
      return false;
    }

    let setCards = mtgJSON[code].tokens.filter(cardData => {
      if (cardData.side && cardData.side != 'a') return false
      return cardData.identifiers.multiverseId == multiverseId
    });
    if (setCards.length === 1) {
      return setCards[0]
    }
    if (setCards.length === 0) {
      return false;
    }
    console.error(`More than one card with Multiverse ID "${multiverseId}" found in ${setName}`);
    return false;
  },

  findSetByUuid: function(uuid) {
    let meta = mtgJSON_meta.uuidToCardMeta[uuid];
    if (typeof meta == 'undefined') {
      console.error(`No meta link for ${uuid}`);
      return false;
    }
    return mtgJSON[meta.set];
  },

  findByUuid: function(uuid) {
    let cardData = this.findCardByUuid(uuid);
    let isToken = false;
    if (cardData === false) {
      cardData = this.findTokenByUuid(uuid);
      if (cardData == false) {
        // Not found as card nor token
        return false;
      }
      // Card is a token
      isToken = true;
    }
    return {
      isToken,
      cardData
    }
  },

  findCardByUuid: function(uuid) {
    let setData = this.findSetByUuid(uuid);
    if (setData === false) return false;
    if (typeof setData == 'undefined') return false;
    for (let i = 0; i < setData.cards.length; i++) {
      let cardData = setData.cards[i];
      if (cardData.identifiers.scryfallId == uuid && (cardData.layout !== 'transform' || cardData.side == 'a')) {
        return Object.assign({}, cardData, { set: { name: setData.name, code: setData.code }});
      }
    }
    return false;
  },

  findTokenByUuid: function(uuid) {
    let setData = this.findSetByUuid(uuid);
    if (setData === false) return false;
    for (let i = 0; i < setData.tokens.length; i++) {
      if (setData.tokens[i].identifiers.scryfallId == uuid) {
        return Object.assign({}, setData.tokens[i], { set: { name: setData.name, code: setData.code }});
      }
    }
    return false;
  },

  mapTcgIds: function() {
    let tcgIds = {};
    Object.keys(mtgJSON).forEach(setKey => {
      mtgJSON[setKey].cards.forEach(card => {
        if (typeof card.identifiers.tcgplayerProductId != 'undefined') {
          tcgIds[parseInt(card.identifiers.tcgplayerProductId)] = card.uuid;
        }
      });
    });
    return tcgIds;
  }

};
