/**
 * Utility functins for helping "formats" scripts.
 */

const csvParser = require('../lib/csvParser'),
  jsonParser = require('../lib/jsonParser')

module.exports = {
  reverseSetLookup: function (sets, query) {
    for (const prop in sets) {
      if (sets[prop] == query) {
        return prop;
      }
    }
    return false;
  },

  parseInputFile: function (inputFile, inputFormat) {
    return csvParser.readCsvFile(inputFile).then(csv => {
      let data = jsonParser.csvToObjects(csv);

      // Now, match up the input data to MTGJSON UUIDs
      let formatIn;
      try {
        formatIn = require('../formats/' + inputFormat).parseInput;
      } catch (err) {
        if (err.code == 'MODULE_NOT_FOUND') {
          return Promise.reject(`Cannot find parser data for format ${inputFormat}`);
        } else {
          return Promise.reject(err);
        }
      }

      return formatIn(data);
    });
  }

};
