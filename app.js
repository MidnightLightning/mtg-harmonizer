/**
 * Main entrypoint.
 *
 * Called as "node app.js commandName", this script will load the appropriate action script
 * and execute its logic.
 */

const actionName = process.argv[2];
if (typeof actionName == 'undefined' || actionName == '') {
  const path = require('path');
  let scriptName = path.basename(process.argv[1]);
  console.log(`Usage: node ${scriptName} actionName`);
  process.exit();
}

let actionFunction;

try {
  actionFunction = require('./actions/' + actionName);
} catch (err) {
  if (err.code == 'MODULE_NOT_FOUND') {
    console.error(`Cannot find action logic for '${actionName}'`);
    console.log(err);
  } else {
    console.log(err);
  }
  process.exit();
}

let rs = actionFunction.apply(null, process.argv.slice(3));
if (typeof rs != 'undefined') {
  // A result was returned
  if (typeof rs.then == 'function') {
    // Promise-like object was returned
    rs.then(rs => {
      if (typeof rs != 'undefined') {
        console.log(rs);
      }
    });
    return;
  }
  // A static result was returned
  console.log(rs);
}
