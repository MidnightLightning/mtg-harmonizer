# Magic: the Gathering Card List Harmonizer

## Assumptions

- **No dependencies**. This is a NodeJS project, so you need that installed (or some sort of container service like Docker, and run Node in a container), but this project will use no NPM package dependencies to keep it light.
- **Scryfall UUIDs as key identifiers**. In order to harmonize across different database identifiers, _some_ identifier needs to be picked as the key one to pass everything else through. Picking Scryfall as the key identifier gives every card a UUID.
- **MTGJSON for offline running**. MTGJSON scrapes the Scryfall API as part of its compilation, so using that data blob allows these scripts to work offline (rather than needing to query API endpoints for every card).

## Prepare MTGJSON data
These scripts use the main `AllPrintings.json` data file from [MTJSON](https://mtgjson.com/) to parse cards from other sources. But not all the data in that JSON blob are needed, so we can skinny it up first, before using it.

Download [the latest version](https://mtgjson.com/downloads/all-files/) of the "AllPrintings" MTGJSON file and put it in the root of this folder. Then, run `node app init`, which will create an `AllPrintings.min.json` and `AllPrintings.meta.json` file in the `lib` folder, which other scripts will use.

## Convert "Have" lists
A "Have" list (or "Tradelist") is a list of distinct cards the user has, so every line of the file must map to an actual card definition. The `convertHaves` action helps do that. The syntax for using it is:

```sh
node app convertHaves input.csv inputFormat outputFormat
```

Valid input and output formats:

- `cardsphere`: Format used by https://www.cardsphere.com (retired; site shutting down)
- `cardtrader`: Format used by https://www.cardtrader.com/
- `deckbox`: Format used by https://deckbox.org. When exporting, choose to include extra columns, and add in the "Image URL" column. Deckbox internally uses the "Multiverse ID" (ID number of the card from Wizards' Gatherer tool) to identify cards, and that ID is part of the URL it generates for that sort of output. When that column is included, the Multiverse ID can be parsed out, and more accurately matched to the Scryfall ID for harmonization.
- `json`: Export the parsed JSON data that's used as an interchange format. This is useful for making a backup of your card inventory, adding in root UUID for each row for anything else wanting to parse it down the road.

## Add new sites
To add a new site parser to the harmonizer, you'll need to create a script in the `/formats` folder with the name of the site, to store card name differences and set name differences that site uses, and input/output logic.

### Variant Visualizing
For each new site added, one of the key areas that will likely need a lot of work is adding in custom mappings for the different card art for cards of the same name. In older sets (namely _Homelands_ and _Alliances_), lots of cards had two or more different artworks, and they weren't assigned collector numbers, so often the best way to tell which one was meant is to look at images of both variants. To help with this, the `showVariants` script can be run to help create a visual guide. Run that script (`node app showVariants`), and it will output a slew of HTML files in a `/variants` folder, one for each set that has cards that have multiple variants. The HTML files have the cards, plus a thumbnail image from Scryfall, and the UUID to use to map it properly.
